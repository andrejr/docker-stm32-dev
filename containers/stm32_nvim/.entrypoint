#!/bin/bash

# Everything in this script has to be run *once* per container instantiation.
# Maybe there's a better way to check if the script already ran, but here we're
# checking for existance of a file in a non-volume directory.

RED=$(tput setaf 1)
readonly RED
YELLOW=$(tput setaf 3)
readonly YELLOW
RESET=$(tput sgr0)
readonly RESET

print_err() {
	printf "${RED}%b${RESET}\n" "$*" >&2
}

print_warn() {
	printf "${YELLOW}%b${RESET}\n" "$*" >&2
}

get_fs_type() {
	local IN_PATH="$1"
	df --output=fstype "$IN_PATH" | sed -n 2p
}

initialize_container() {
	# change the UID and GID of our Docker user to the external user
	fixuid

	# mounting ssh keys
	# -----------------
	local SSHPATH="$HOME/.ssh"
	local SSH_MOUNT_PATH="$HOME/.secrets/ssh"
    local WARN

	if [ -d "$SSH_MOUNT_PATH" ]; then
		echo "Mounting ssh keys..."
		cp -R "${SSH_MOUNT_PATH}/" "${SSHPATH}/"
		chmod 700 "${SSHPATH}"
		chmod 644 "${SSHPATH}/id_rsa.pub"
		chmod 600 "${SSHPATH}/id_rsa"
		echo "SSH keys mounted!"
	else
		WARN=''
		WARN+='No ssh mount.\n'
		WARN+='To mount your SSH keys in the container, you should probably '
		WARN+='add the following argument to "docker run":\n\n'
		WARN+="    -v \$HOME/.ssh:$SSH_MOUNT_PATH \n\n"
		WARN+=', assuming your SSH folder is ~/.ssh\n'
		print_warn "$WARN"
	fi

	# stowing dotfiles
	# ----------------
	local STOW_DOTFILES="$HOME/dotfiles"
	local STOW_TARGET_SYMLINK="$HOME/.config"
	local STOW_TARGET_INTERMEDIATE
	STOW_TARGET_INTERMEDIATE=$(realpath "$STOW_TARGET_SYMLINK")
	local STOW_TARGET=${STOW_TARGET_INTERMEDIATE%/*} || print_err \
		"Error calculating stow target!" && false
	if [ -d "$STOW_DOTFILES" ]; then
		echo "Stowing volume-mounted dotfiles..."
		cd "$STOW_DOTFILES" || exit
		find "$STOW_DOTFILES" -maxdepth 1 -mindepth 1 -type d \
			-printf "%f\0" \
			| xargs -0 -n1 -i{} stow -vS "{}" -t "$STOW_TARGET" 2>/dev/null
		cd - || exit
		echo -e "Done stowing volume-mounted dotfiles!\n"
	else
        WARN=' ---------------------------------\n\n'
		WARN+='You haven'\''t mounted a dotfiles directory to be stowed. '
		WARN+='To do so, pass the following arg to "docker run":\n\n'
		WARN+="    -v /path/to/your/dotfiles:$STOW_DOTFILES\n\n"
		WARN+=", or mount single dotfiles folders like this:\n\n"
		WARN+="    -v /path/to/your/dotfiles/nvim:$STOW_DOTFILES/nvim\n"
		print_warn "$WARN"
	fi

	# warnings
	# --------
	if [ "$(get_fs_type "/tmp/")" != 'tmpfs' ]; then
        WARN=' ---------------------------------\n\n'
		WARN+='Your "/tmp" folder isn'\'' a tmpfs mount. '
		WARN+='This may lead to reduced performance.\n'
		WARN+='To provide a tmpfs for "/tmp", please append the '
		WARN+='following argument to your "docker run" command: \n\n'
		WARN+='    --tmpfs /tmp/\n'
		print_warn "$WARN"
	fi

	local PERSIST="$HOME/.persist"
	readonly PERSIST
	if ! [ -d "$PERSIST" ]; then
        WARN=' ---------------------------------\n\n'
		WARN+="You're running this container without a \"$PERSIST\" mount. "
		WARN+='Such a mount is pretty convenient, because it allows you to '
		WARN+='cache your vim plugin installs and such, which speeds up '
		WARN+='its first run.\n'
		WARN+='It caches ~/.config and ~/.local\n'
		print_warn "$WARN"
	fi

	local GITCONFIG="$HOME/.gitconfig"
	readonly GITCONFIG

	if ! [ -s "$GITCONFIG" ]; then
        WARN=' ---------------------------------\n\n'
		WARN+='You haven'\''t mounted your .gitconfig file to the container. '
		WARN+='To do so, pass the following arg to "docker run":\n\n'
		WARN+='    --mount type=bind,'
		WARN+="source=\"\$HOME/.gitconfig\",target=$HOME/.gitconfig\n"
		WARN+="\n, assuming your .gitconfig file is in \$HOME.\n"
		print_warn "$WARN"
	fi

	local WORKSPACE="$HOME/workspace"
	readonly WORKSPACE

	if ! [ -d "$WORKSPACE" ]; then
        WARN=' ---------------------------------\n\n'
		WARN+='You haven'\''t mounted a workspace. '
		WARN+='To do so, pass the following arg to "docker run":\n\n'
		WARN+="    -v /path/to/your/project:$WORKSPACE"
		print_warn "$WARN"
	fi
}

# if this file exists, then the container has already been initialized
CHECKFILE=$HOME/.container-initialized
if ! [ -f "$CHECKFILE" ]; then
	initialize_container \
		&& touch "$CHECKFILE"
fi
