#
# Defines environment variables.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

#
# Editors
#

export VISUAL="nvim"
export EDITOR="nvim"
export PAGER='less'

path=(
  /usr/local/{bin,sbin}
  /home/$(whoami)/.local/bin
  $path
)

# Ensure that a non-login, non-interactive shell has a defined environment.
if [[ ("$SHLVL" -eq 1 && ! -o LOGIN) && -s \
	"${ZDOTDIR:-$HOME}/.zprofile" ]]; then
	source "${ZDOTDIR:-$HOME}/.zprofile"
fi

#
# Development-related
#

export MAKEFLAGS="-j$(nproc)"
export CPPUTEST_HOME="/home/$(whoami)/devel/libs/cpp/cpputest"
